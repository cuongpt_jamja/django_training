### Master branch
- Install virtualenv
- Install Django
- Add gitignore, README
- After each step, each branch will merge into master
### Step 1: Create project (create_project branch)
- Project folder name: project
- Config database: keep default SqLite3 database
- Run Django server test:
`python manage.py runserver`
- **Worked**
### Step 2: Create and activate model (create_model branch)
- Create new folder for new app in the same directory with `manage.py`
- In the new app `Polls` will have 2 models: **Question** and **Choice**.
 A **Question** will have question and publication date; **Choice** have text of choice and vote tally for voting
- Question model has 2 class variables (attribute): question_text, pub_date
- Choice model has 3 attributes: question (Foreign Key), choice_text, votes
- Then activate model by add `polls` app to installed_apps in settings.py
- Makemigrations to update changes in models
`python manage.py makemigrations polls` to create migrations for changes
- `python manage.py migrate` to apply those changes to the database
### Step 3: Admin site (admin-site branch)
- Create superuser/admin by `python manage.py createsuperuser`
- Register models into admin.py; so in admin dashboard we are able to edit the database
- For modifying admin site interface, structure, we will do it later on after learn about view
### Step 4: Edit view (view branch)
### Step 5: Add form for voting in each question (post-request branch)
- Add a form with POST method to send vote result